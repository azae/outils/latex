FROM debian:testing
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
 && apt-get install -y --no-install-recommends -q \
    fonts-font-awesome \
    fonts-lato \
    fonts-roboto \
    inkscape \
    latex-cjk-all \
    latex2rtf \
    latexmk \
    make \
    pandoc \
    python3-pygments \
    tex-gyre \
    texlive-base \
    texlive-extra-utils \
    texlive-fonts-extra \
    texlive-fonts-recommended \
    texlive-lang-english \
    texlive-lang-european \
    texlive-lang-french \
    texlive-lang-other \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-latex-recommended \
    texlive-luatex \
    texlive-pictures \
    tipa \
 && apt-get clean \
 && rm -rf /usr/share/man /usr/share/doc /var/lib/apt/lists/*
ENV TEXINPUTS :/usr/local/share/latex/sty:/usr/local/share/latex/common
COPY common /usr/local/share/latex/common/
COPY sty /usr/local/share/latex/sty/
COPY run /usr/local/bin/
COPY run /usr/local/bin/asuser
RUN chmod +x /usr/local/bin/run /usr/local/bin/asuser
COPY fonts/khand /usr/share/fonts/truetype/
COPY fonts/poppins /usr/share/fonts/truetype/
# Build cache of fonts
COPY tests /tmp/tests/
RUN cd /tmp/tests/ && make clean pdf
RUN cd /tmp/tests/ && make clean && lualatex *.tex
ENTRYPOINT ["/usr/local/bin/run"]
VOLUME ["/data"]
