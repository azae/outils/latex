# [LaTeX container](https://gitlab.com/azae/outils/latex)

This is our [LaTeX](https://fr.wikipedia.org/wiki/LaTeX) container used to build all enterprises documents like invoice, quote, presentations and business propositions.

This repo contain our container description, our custom styles to buid documents and somes predefined values to simplified our work.

# Usage

## cli

use the `/data` volume to mount your tex directory and run :

    docker run --rm -it \
      -v $(pwd)/tests/:/data \
      registry.gitlab.com/azae/outils/latex/cli \
      pdflatex file.tex file.pdf

or if your LaTeX source directory is managed by Makefile :

    docker run --rm -it -v $(pwd):/data registry.gitlab.com/azae/outils/latex/cli make pdf

or
    docker run --rm -it -v $(pwd):/data registry.gitlab.com/azae/outils/latex asuser make pdf


You can also use `inotify-hookable` to trigger the build of your pdf on .tex file update.

    inotify-hookable -w . -c "docker run --rm -it -v $(pwd):/data registry.gitlab.com/azae/outils/latex/cli make pdf"

Note : it's note necessary to specify user and group, because the entry point automaticaly check the proprietary of /data

## gitlab-ci

You can use this container to build your latex documents with gitlab pipeline, just use in your `.gitlab-ci.yml` like this :

    build-pdf:
      image: registry.gitlab.com/azae/outils/latex/gitlab
      script:
        - make pdf
      artifacts:
        paths:
          - ...


# LaTeX sty subdirectory

Zoom on somes [LaTeX](https://fr.wikipedia.org/wiki/LaTeX) .sty file :

## Beamer

**beamerthemeAzae.sty** : Le Theme [Beamer](https://www.ctan.org/pkg/beamer) d'[Azaé](http://azae.net)

## Factures et devis

**devis.sty / facture.sty** : Packages de création de devis et de factures Françaises.

Les fichiers de devis et de factures doivent être nommés en respectant les règles suivantes : `numero_nom-du-document.tex` avec un `_` comme séparateur entre le numéro et le nom du document.

Parmis les nombreuses options disponibles pour utiliser le style de facture il y a :

- `\def\FactureAcquittee` à positionner à `oui` ou `non`, permet de produire une facture avec un reste à payer de 0.
- `\def\FactureAvoir` à positionner à `oui` ou `non`, permet d'éditer une facture d'avoir.
- `\tva{20}` défini le taux de TVA, 0 pour les auto-entrepreneur ou pour l'export en europe.


## Les couleurs Tango

**tango.sty** : Definition of [Tango](http://tango.freedesktop.org/) colors and update of header to use this colors. You can also use color name like `\color{DarkScarletRed}`.

## sty usage

You can Clone this repo and add sty path in TEXINPUTS environement variable like

    git clone https://gitlab.com/azae/outils/latex.git ~/latex
    echo 'TEXINPUTS=$TEXINPUTS:~/latex/sty/' >> ~/.bashrc
